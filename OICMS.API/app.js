﻿var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require("morgan");
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');
var LocalStrategy = require('passport-local').Strategy;
var expressValidator = require('express-validator');
var routes = require('./routes/index');
var users = require('./routes/users');
var admin = require('./routes/admin');
var settings = require('./routes/settings');
var skipschedule = require('./routes/skipschedule');
var createschedule = require('./routes/createschedule');
var applyschedule = require('./routes/applyschedule');
var schedules = require('./routes/schedules');
var http = require('http');
var mongoose = require('mongoose');
var conn = mongoose.connection;
var app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


mongoose.connect('mongodb://localhost/oicms');
//mongoose.connect('mongodb://oicms:sachin225@ds055594.mlab.com:55594/oicms');
//mongoose.connect('mongodb://oicms_user:Varahitech#3@ds064188.mlab.com:64188/oicms');


conn.on('error', console.error.bind(console, 'connection error:'));
conn.once('open', function () {
    console.log("successful");
});
app.use(morgan('dev'));
app.set('port', process.env.PORT || 3001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('public'));
app.use(session({
    secret: 'oicmssecret',
    saveUninitialized: true,
    resave: true,
    name : 'expresscookie'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
      , root = namespace.shift()
      , formParam = root;
        
        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param : formParam,
            msg   : msg,
            value : value
        };
    }
}));
app.use(flash());
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});
app.use('/', routes);
app.use('/users', users);
app.use('/admin', admin);
app.use('/settings', settings);
app.use('/schedules', schedules);
app.use('/skipschedule', skipschedule);
app.use('/createschedule', createschedule);
app.use('/applyschedule', applyschedule);

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
