﻿angular.module("oicmsAdmin", []).controller('customers', ['$scope', '$http', function ($scope, $http) {
        $scope.loading = false;
        $scope.fert = false;
        $scope.showlist = true;
        
        $scope.showaddform = function () {
            $scope.customer = {
                tanks: [
            {
                tankname : 'Tank 1',
                tankswitch: false,
                agitator: false
            },
            {
                tankname : 'Tank 2',
                tankswitch: false,
                agitator: false
            },
            {
                tankname : 'Tank 3',
                tankswitch: false,
                agitator: false
            },
            {
                tankname : 'Tank 4',
                tankswitch: false,
                agitator: false
            },
            {
                tankname : 'Tank 5',
                tankswitch: false,
                agitator: false
            },
            {
                tankname : 'Tank 6',
                tankswitch: false,
                agitator: false
            },
            {
                tankname : 'Tank 7',
                tankswitch: false,
                agitator: false
            }
        ],
                slaves: [
                    {
                        location: 'Location 1',
                        pumpcard: false,
                        filtercard : 0,
                        CardsFirst : 20,
                        pfccards : 0,
                    }
                ],
                contents : [{ value : 'N' }, { value : 'P' }, { value : 'K' }]
            };
            $scope.custForm.$setPristine();
            $scope.showlist = false;
            $scope.showusername = false;
        }
        $scope.showList = function () {
            $scope.showlist = true;
            $('a[href="#master"]').trigger('click');
        }
        $scope.addSlave = function () {
            $scope.customer.slaves.push({
                location: '',
                pumpcard: false, filtercard: 0,
                CardsFirst : 20,
                pfccards : 0
            });
        }
        $scope.removeSlave = function (index) {
            $scope.customer.slaves.splice(index, 1);
        }
        
        $scope.CardValueChanged = function (CardRadioValue, index){
            var in1 = '.in1' + index;
           if (CardRadioValue == 1) {
                $(in1).children('input.form-control').removeAttr('readonly');

            }
            else if (CardRadioValue == 2) {
                $scope.customer.slaves[index].CardsFirst = 20;
                $scope.customer.slaves[index].CardsSecond = 21;
                $(in1).children('input.form-control').prop('readonly','readonly');
            }
        }
        
        $scope.checkValves = function (index, id) {
            var idx = parseInt(index);                      
            if (id == 1) {
                cardfirst = $scope.customer.slaves[idx].CardsFirst;
                if (cardfirst == undefined)
                    $scope.customer.slaves[idx].CardsFirst = 20;
            }
            else if (id == 2) {
                cardsecond = $scope.customer.slaves[idx].CardsSecond; 
                if (cardsecond == undefined)
                    $scope.customer.slaves[idx].CardsSecond = 21;
            }
        }

        $scope.pushContent = function () {
            for (var i = 0; i < $scope.customer.contents.length; i++) {
                if ($scope.customer.contents[i].value == "" || $scope.customer.contents[i].value == null || $scope.customer.contents[i].value == undefined) {
                    alertify.alert("Not Allowed! Fill all the fields");
                    return;
                }
            }
            $scope.customer.contents.push({ value : '' });
        };
        $scope.removeContent = function (index) {
            $scope.customer.contents.splice(index, 1);
        }
        $scope.tankEnableSwitch = function (cindex) {
            if (!$scope.customer.tanks[cindex].tankswitch)
                $scope.customer.tanks[cindex].agitator = false;
            var agitator = '#ag' + cindex;
            $(agitator).toggleClass('disabled');
        }      
        $scope.checkContent = function (index) {
            for (var i = 0; i < $scope.customer.contents.length; i++) {
                if (i != index && $scope.customer.contents[index].value != "" && $scope.customer.contents[i].value == $scope.customer.contents[index].value) {
                    alertify.alert("Already Present");
                    $scope.customer.contents[index].value = "";
                    return;
                }
            }
        }

        $("#username").blur(function () {
            if ($scope.customer.username != null) {
                if ($scope.customer.username.length < 6) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            $('.userwarning').css('color', 'red');
                            $scope.msg = "Username must be atleast 6 characters long";
                            $scope.showusername = true;
                            $scope.validuser = false;
                        })
                    }, 100);
                    return;
                }
                $http.post('/users/checkusername', { username: $scope.customer.username }).success(function (result, err) {
                    if (result) {
                        $('.userwarning').css('color', 'red');
                        $scope.msg = "Username already exists!";
                        $scope.showusername = true;
                        $scope.validuser = false;
                    }
                    else {
                        $('.userwarning').css('color', 'green');
                        $scope.msg = "Username Available!";
                        $scope.showusername = true;
                        $scope.validuser = true;
                    }
                }).error(function (err) {
                    return err;
                });
            }
        });
        $("#username").focus(function () {
            setTimeout(function () {
                $scope.$apply(function () { $scope.showusername = false; });
            }, 100);
        });
        $scope.deleteThis = function (id) {
            alertify.set({ labels: { ok: "Yes", cancel: "No" } });
            alertify.confirm("Are you sure to delete this customer?", function (e) {
                if (!e) {
                          
                }
                else return;
            });
        }
        $scope.validateForm = function () {
            if(!$scope.custForm.$valid)
            $('a[href="#master"]').trigger('click');
        }

        $scope.saveCustomer = function () {
            if ($scope.validuser) {
                if ($scope.customer.password.length < 8) {
                    alertify.alert("Password must be atleast 8 characters long!");
                    return;
                }
                if ($scope.customer.slaves.length == 0) { 
                    alertify.alert("Please add atleast one slave!");
                    return;
                }              
                for (var i = 0; i < $scope.customer.slaves.length; i++) {
                    pumpenabled = [];
                    pumpnames = [];
                    if ($scope.customer.slaves[i].location == null || $scope.customer.slaves[i].location == '') {
                        alertify.alert("Please enter slave location name at index " + (i + 1));
                        return;
                    }
                    if ((parseInt($scope.customer.slaves[i].filtercard) == 0 || $scope.customer.slaves[i].filtercard == undefined) && $scope.customer.slaves[i].pfccards == 0 && !$scope.customer.slaves[i].pumpcard) {
                        alertify.alert("Please add atleast one Pump or Filter or PFC Card at index " + (i + 1));
                        return;
                    }
                    if ($scope.customer.slaves[i].pumpcard) {
                        if (!$scope.customer.slaves[i].pump1 && !$scope.customer.slaves[i].pump2 && !$scope.customer.slaves[i].pump3 && !$scope.customer.slaves[i].pump4) {
                            alertify.alert("Please enable atleast one Pump at index " + (i + 1));
                            return;
                        }
                        else {
                            if ($scope.customer.slaves[i].pump1) {
                                pumpenabled.push(1);
                                pumpnames.push("Pump1");
                            }
                            if ($scope.customer.slaves[i].pump2) {
                                pumpenabled.push(2);
                                pumpnames.push("Pump2");
                            }
                            if ($scope.customer.slaves[i].pump3) {
                                pumpenabled.push(3);
                                pumpnames.push("Pump3");
                            }
                            if ($scope.customer.slaves[i].pump4) {
                                pumpenabled.push(4);
                                pumpnames.push("Pump4");
                            }
                            $scope.customer.slaves[i].pumpenabled = pumpenabled;
                            $scope.customer.slaves[i].pumpnames = pumpnames;
                        }
                    }
                    if ($scope.customer.slaves[i].pfccards == 0)
                        $scope.customer.slaves[i].valveenabled = 0;
                    else if ($scope.customer.slaves[i].pfccards == 1)
                        $scope.customer.slaves[i].valveenabled = $scope.customer.slaves[i].CardsFirst;
                    else if ($scope.customer.slaves[i].pfccards == 2)
                        $scope.customer.slaves[i].valveenabled = $scope.customer.slaves[i].CardsSecond;
                }
                if ($scope.customer.fertigation) {
                    for (var i = 0; i < $scope.customer.contents.length; i++) {
                        if ($scope.customer.contents[i].value == "" || $scope.customer.contents[i].value == null || $scope.customer.contents[i].value == undefined) {
                            alertify.alert("Please fill all the contents fields!");
                            return;
                        }
                    }
                }

                $scope.loading = true;
                $http.post('/users/newuser', $scope.customer).success(function (result, err) {
                    if (result.data) {
                        alertify.alert("Customer added successfully!");
                        window.location.reload();
                    }
                    else {
                        alertify.alert("Error!");
                        window.location.reload();
                    }
                }).error(function (err) {
                    $scope.loading = false;
                    return err;
                });
            }
            else {
                alertify.alert("Please enter valid username");
                return;
            }
        }
    }]);
