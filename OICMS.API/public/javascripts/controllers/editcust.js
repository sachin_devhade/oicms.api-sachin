﻿angular.module("oicmsAdmin", []).controller('editcustomer', ['$scope', '$http', function ($scope, $http) {
    $scope.loading = false;
    var temp = abcd;
    var cust = JSON.parse(temp);
    cust.zipcode = parseInt(cust.zipcode);
        for (var i = 0; i < cust.slaves.length; i++) {
            if (cust.slaves[i].valveenabled > 20) {
                cust.slaves[i].CardsFirst = 20;
                cust.slaves[i].CardsSecond = cust.slaves[i].valveenabled;
            }
            else {
                if (cust.slaves[i].pfccards == 0) {
                    cust.slaves[i].CardsFirst = 20;
                    cust.slaves[i].CardsSecond = 21;
                }
                else {
                    cust.slaves[i].CardsFirst = cust.slaves[i].valveenabled;
                    cust.slaves[i].CardsSecond = 21;
                }
            }
            for (var j = 0; j < cust.slaves[i].pumpenabled.length; j++) {
                if (cust.slaves[i].pumpenabled[j] == 1)
                    cust.slaves[i].pump1 = true;
                else if (cust.slaves[i].pumpenabled[j] == 2)
                    cust.slaves[i].pump2 = true;
                else if (cust.slaves[i].pumpenabled[j] == 3)
                    cust.slaves[i].pump3 = true;
                else if (cust.slaves[i].pumpenabled[j] == 4)
                    cust.slaves[i].pump4 = true;
            }
        }
    $scope.customer = angular.copy(cust);
        $scope.addSlave = function () {
            $scope.customer.slaves.push({
                location: '',
                pumpcard: false, filtercard: 0,
                CardsFirst : 20,
                pfccards : 0
            });
        }
        $scope.removeSlave = function (index) {
            $scope.customer.slaves.splice(index, 1);
        }
        
        $scope.CardValueChanged = function (CardRadioValue, index) {
            var in1 = '.in1' + index;
            if (CardRadioValue == 2) {
                $scope.customer.slaves[index].CardsFirst = 20;
                $scope.customer.slaves[index].CardsSecond = 21;
            }
        }
        
        $scope.checkValves = function (index, id) {
            var idx = parseInt(index);
            if (id == 1) {
                cardfirst = $scope.customer.slaves[idx].CardsFirst;
                if (cardfirst == undefined)
                    $scope.customer.slaves[idx].CardsFirst = 20;
            }
            else if (id == 2) {
                cardsecond = $scope.customer.slaves[idx].CardsSecond;
                if (cardsecond == undefined)
                    $scope.customer.slaves[idx].CardsSecond = 21;
            }
        }
        
        $scope.pushContent = function () {
            for (var i = 0; i < $scope.customer.contents.length; i++) {
                if ($scope.customer.contents[i].value == "" || $scope.customer.contents[i].value == null || $scope.customer.contents[i].value == undefined) {
                    alertify.alert("Not Allowed! Fill all the fields");
                    return;
                }
            }
            $scope.customer.contents.push({ value : '' });
        };
        $scope.removeContent = function (index) {
            $scope.customer.contents.splice(index, 1);
        }
        $scope.tankEnableSwitch = function (cindex) {
            if (!$scope.customer.tanks[cindex].tankswitch)
                $scope.customer.tanks[cindex].agitator = false;
            var agitator = '#ag' + cindex;
            $(agitator).toggleClass('disabled');
        }
        $scope.checkContent = function (index) {
            for (var i = 0; i < $scope.customer.contents.length; i++) {
                if (i != index && $scope.customer.contents[index].value != "" && $scope.customer.contents[i].value == $scope.customer.contents[index].value) {
                    alertify.alert("Already Present");
                    $scope.customer.contents[index].value = "";
                    return;
                }
            }
        }
        $scope.saveCustomer = function () {
            if ($scope.customer.slaves.length == 0) {
                alertify.alert("Please add atleast one slave!");
                return;
            }
            for (var i = 0; i < $scope.customer.slaves.length; i++) {                
                pumpenabled = [];
                pumpnames = [];
                if ($scope.customer.slaves[i].location == null || $scope.customer.slaves[i].location == '') {
                    alertify.alert("Please enter slave location name at index " + (i + 1));
                    return;
                }
                if ((parseInt($scope.customer.slaves[i].filtercard) == 0 || $scope.customer.slaves[i].filtercard == undefined) && $scope.customer.slaves[i].pfccards == 0 && !$scope.customer.slaves[i].pumpcard) {
                    alertify.alert("Please add atleast one Pump or Filter or PFC Card at index " + (i + 1));
                    return;
                }
                if ($scope.customer.slaves[i].pumpcard) {
                    if (!$scope.customer.slaves[i].pump1 && !$scope.customer.slaves[i].pump2 && !$scope.customer.slaves[i].pump3 && !$scope.customer.slaves[i].pump4) {
                        alertify.alert("Please enable atleast one Pump at index " + (i + 1));
                        return;
                    }
                    else {
                        if ($scope.customer.slaves[i].pump1) {
                            pumpenabled.push(1);
                            pumpnames.push("Pump1");
                        }
                        if ($scope.customer.slaves[i].pump2) {
                            pumpenabled.push(2);
                            pumpnames.push("Pump2");
                        }
                        if ($scope.customer.slaves[i].pump3) {
                            pumpenabled.push(3);
                            pumpnames.push("Pump3");
                        }
                        if ($scope.customer.slaves[i].pump4) {
                            pumpenabled.push(4);
                            pumpnames.push("Pump4");
                        }
                        $scope.customer.slaves[i].pumpenabled = pumpenabled;
                        $scope.customer.slaves[i].pumpnames = pumpnames;
                    }
                }
                else { 
                    $scope.customer.slaves[i].pumpenabled = [];
                    $scope.customer.slaves[i].pumpnames = [];
                }
                if ($scope.customer.slaves[i].pfccards == 0)
                    $scope.customer.slaves[i].valveenabled = 0;
                else if ($scope.customer.slaves[i].pfccards == 1)
                    $scope.customer.slaves[i].valveenabled = $scope.customer.slaves[i].CardsFirst;
                else if ($scope.customer.slaves[i].pfccards == 2)
                    $scope.customer.slaves[i].valveenabled = $scope.customer.slaves[i].CardsSecond;
            }
            if ($scope.customer.fertigation) {
                for (var i = 0; i < $scope.customer.contents.length; i++) {
                    if ($scope.customer.contents[i].value == "" || $scope.customer.contents[i].value == null || $scope.customer.contents[i].value == undefined) {
                        alertify.alert("Please fill all the contents fields!");
                        return;
                    }
                }
            }            
            $scope.loading = true;
            $http.post('/users/updateuser', $scope.customer).success(function (result, err) {
                if (result.data) {
                    alertify.alert("Customer updated successfully!");
                    window.location = "/customers";
                }
                else {
                    alertify.alert("Error!");
                    window.location = "/customers";
                }
            }).error(function (err) {
                $scope.loading = false;
                return err;
            });
    }
}]);