﻿var Schedule = require('../models/schedule.server.model.js');

exports.createSchedule = function (req, res) 
{
    var obj = req.body;
    
    //req.body consists of JSON object of schedule schema

    obj.status = "ToCreate";    
    var newSchedule = new Schedule(obj);
    newSchedule.save(function (error, schedule, total) {
        if (error) { 
            return res.json(false);
        }
        else if (schedule) { 
            return res.json(true);
        }
        else
            return res.json(false);
    });
}

//exports.created = function (req,res) 
//{
//    Schedule.findOneAndUpdate({ 'masterId' : req.params.masterId, $where : "this.scheduleNum.toString() == " + req.params.scheduleNum + "" },
//      { $set: { "status": "created" } },
//        { new: true },
//        function (err, doc) {
//        if (err)
//            res.send(err);
//        else
//            res.send(doc);
//    });
//}

exports.applySchedule = function (req, res) 
{
    var object = req.body;
    
    //req.body consists of JSON array for ex : [{monthYear: '', dates: '',backflush: ''},{monthYear: '', dates: '',backflush: ''},....]

    Schedule.findOneAndUpdate({ 'masterId' : req.params.masterId, 'status': 'Created', 'scheduleNum' : req.params.scheduleNum},
        {
        $set: { "status": "ToApply" } ,
        $push: {
            applyingDates: {
                $each: object
            }
        }
        },
        { new: true },
        function (err, doc) {
        if (err)
            res.json(false);
        else if (doc)
            res.json(true);
        else
            res.json(false);
    });
}

exports.skipSchedule = function (req, res) {
    var object = req.body;
    Schedule.findOneAndUpdate({ 'masterId' : req.params.masterId, 'status': 'Applied', 'scheduleNum' : req.params.scheduleNum },
         {
        $set: { "applyingDates": [] } 
    },
        { new: true },
        function (err, doc) {
        if (err)
            res.json(false);
        else if (doc)
            Schedule.findOneAndUpdate({ 'masterId' : req.params.masterId, 'status': 'Applied', 'scheduleNum' : req.params.scheduleNum },
         {
                $push: {
                    applyingDates: {
                        $each: object
                    }
                }
            },
        { new: true },
        function (err, doc) {
                if (err)
                    res.json(false);
                else if (doc)
                    res.json(true);
                else
                    res.json(false);
            });
        else
            res.json(false);
    });
}


//exports.applied = function (req, res) {
//    Schedule.findOneAndUpdate({ 'masterId' : req.params.masterId, $where : "this.scheduleNum.toString() == " + req.params.scheduleNum + "" },
//      { $set: { "status": "applied" } },
//        { new: true },
//        function (err, doc) {
//        if (err)
//            res.send(err);
//        else
//            res.send(doc);
//    });
//}

//exports.skipped = function (req, res) {
//    Schedule.findOneAndUpdate({ 'masterId' : req.params.masterId, $where : "this.scheduleNum.toString() == " + req.params.scheduleNum + "" },
//      { $set: { "status": "skipped" } },
//        { new: true },
//        function (err, doc) {
//        if (err)
//            res.send(err);
//        else
//            res.send(doc);
//    });
//}