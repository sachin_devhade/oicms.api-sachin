
var User = require('../models/user.server.model.js');
var Pumps = require('../models/pumps.server.model.js');

exports.createUser = function (req, res) {
    var obj = req.body;
    obj.powerStatus = "on";
    var entry = new User(obj);
    entry.save(function (err) {
        if (err)
            res.json({ data : false });
        else {
            res.json({ data : true });
        }
    });
}

exports.updateUser = function (req, res) {    
    User.remove({ _id: req.body._id }, function (err) {
        if (!err) {
            var entry = new User(req.body);
            entry.save(function (err) {
                if (err)
                    res.json({ data : false });
                else {
                    res.json({ data : true });               
                }
            });
        }
        else {
            res.json({ data : false });
        }
    });  
}

exports.editSlave = function (req, res) {
    User.findOneAndUpdate({ 'slaves._id' : req.body.slaveId},
      { $set: { "slaves.$.location": req.body.location } },
        { new: true },
        function (err, doc) {
        if (doc)
            res.send(true);
        else if(err)
            res.send(false);
    });
}

exports.editPump = function (req, res) {
    Pumps.findOne({ masterId : req.body.masterId }, function (err, pump) {
        if (pump) {
            var pumpname = pump.names.split(',');
            var names = "";
            for (var i = 0; i < pumpname.length ; i++) {
                if (i+1 == parseInt(req.body.pumpNum))
                    names += req.body.name + ",";
                else
                    names += pumpname[i] + ",";
            }
            names = names.substr(0, names.length - 1);
        Pumps.findOneAndUpdate({ 'masterId' : req.body.masterId },
      { $set: { "names": names } },
        { new: true },
        function (error, doc) {
                if (doc)
                    res.send(true);
                else if (err)
                    res.send(false);
            });
        }
        else { 
            res.send(false);
        }
    });
   
}


