﻿var Sources = require('../models/sources.server.model.js');
var Plots = require('../models/plots.server.model.js');
var Slaves = require('../models/slaves.server.model.js');
var mongoose = require('mongoose');
exports.saveSetting = function (req, res) {
    var obj = req.body;
    var newSetting = new Setting(obj);
    newSetting.save();
}

exports.saveSource = function (req, res) {
    var obj = req.body;
    if (!obj._id) { 
        obj._id = new mongoose.mongo.ObjectID();
    }
    Sources.findOneAndUpdate({ '_id' : obj._id},
        {
        $set: {
            "masterId" : obj.masterId,
            "name" : obj.name,
            "pumps" : obj.pumps
        }
    },
        { new: true, upsert: true },
        function (err, doc) {
        if (err)
            res.json(false);
        else if (doc)
            res.json(true);
        else
            res.json(false);
    });
}

exports.savePlot = function (req, res) {
    var obj = req.body;
    if (!obj._id) {
        obj._id = new mongoose.mongo.ObjectID();
    }
    Plots.findOneAndUpdate({ '_id' : obj._id },
        {
        $set: {
            "masterId" : obj.masterId,
            "sourceIds" : obj.sourceIds,
            "name" : obj.name,
            "crop" : obj.crop,
            "plotType" : obj.plotType,
            "acre" : obj.acre,
            "prudate" : obj.prudate,
            "drip" : obj.drip,
            "totalValves" : obj.totalValves,
        }
    },
        { new: true, upsert: true },
        function (err, doc) {
        if (err)
            res.json(false);
        else if (doc)
            res.json(true);
        else
            res.json(false);
    });
}

exports.updatestatus = function (req, res) {
    Slaves.findOne({ 'slaveId' : req.body.slaveId,'scheduleNum' : req.body.scheduleNum },
        function (err, slave) {
        if (slave) {
            if (req.body.startTime !== undefined)
                slave.startTime = req.body.startTime;
            if (req.body.remainingTime !== undefined)
                slave.remainingTime = req.body.remainingTime;
            if (req.body.backflushTime !== undefined)
                slave.backflushTime = req.body.backflushTime;
            if (req.body.pwrStatus !== undefined)
                slave.pwrStatus = req.body.pwrStatus;
            if (req.body.pwrStatusTime !== undefined)
                slave.pwrStatusTime = req.body.pwrStatusTime;
            if (req.body.groupNum !== undefined)
                slave.groupNum = req.body.groupNum;
            if (req.body.valveNums !== undefined)
                slave.valveNums = req.body.valveNums;
            if (req.body.groupRuntime !== undefined)
                slave.groupRuntime = req.body.groupRuntime;
            if (req.body.sendTime !== undefined)
                slave.sendTime = req.body.sendTime;
            Slaves.remove({ _id : slave._id }, function (err) {
                if (!err) {
                    var newActivate = new Slaves({
                        _id : slave._id,
                        slaveId : slave.slaveId,
                        scheduleNum : slave.scheduleNum,
                        startTime : slave.startTime,
                        remainingTime : slave.remainingTime,
                        backflushTime : slave.backflushTime,
                        pwrStatus : slave.pwrStatus,
                        pwrStatusTime : slave.pwrStatusTime,
                        groupNum : slave.groupNum,
                        valveNums : slave.valveNums,
                        groupRuntime : slave.groupRuntime,
                        sendTime : slave.sendTime
                    });
                    newActivate.save(function (err) {
                        if (err)
                            res.json(false);
                        else
                            res.json(true);
                    });
                }
                else 
                    res.json(false);
            });
        }
        else 
            res.json(false);
    });
}

exports.activate = function (req, res) {
    var obj = req.body;
    var newActivate = new Slaves(obj);
    newActivate.save(function (err) { 
        if (err)
            res.json(false);
        else
            res.json(true);
    });
}