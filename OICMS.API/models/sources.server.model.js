﻿var mongoose = require('mongoose');
var schema = mongoose.Schema;

var Sources = new schema({
    masterId : String,
    name : String,
    pumps : String,
});

module.exports = mongoose.model('source', Sources);