/**
 * Created by user on 5/11/2016.
 */
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var userSchema = new schema({
    username : String,
    password : String,
    name : String,
    email : String,
    address : String,
    mobile : String,
    city : String,
    state : String,
    zipcode : String,    
    powerStatus : String,
    fertigation : Boolean,
    tanks : [{
            tankname : String,
            tankswitch : Boolean,
            agitator : Boolean
        }],
    contents : [{value : String, _id : false}],
    slaves : [{
            location : String,
            pumpcard : Boolean,
            filtercard : Number,
            pfccards : Number,
            pumpenabled: [Number],
            pumpnames : [String],
            valveenabled : Number
        }]
});


var User = module.exports = mongoose.model('user', userSchema);

module.exports.getUserByUsername = function (username, callback) {
    var query = { username: username };
    User.findOne(query, callback);
}
