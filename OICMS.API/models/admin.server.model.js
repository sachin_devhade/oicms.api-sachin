﻿var mongoose = require('mongoose');
var schema = mongoose.Schema;
var bcrypt = require('bcryptjs');

var Admin = new schema({
    username: String,
    password: String,
});


var Admin = module.exports = mongoose.model('admin', Admin);

module.exports.getUserByUsername = function (username, callback) {
    var query = { username: username };
    Admin.findOne(query, callback);
}

module.exports.getUserById = function (id, callback) {
    Admin.findById(id, callback);
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
        if (err) throw err;
        callback(null, isMatch);
    });
}