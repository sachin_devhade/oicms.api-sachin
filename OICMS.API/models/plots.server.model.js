﻿var mongoose = require('mongoose');
var schema = mongoose.Schema;

var Plots = new schema({
    masterId : String,
    sourceIds : String,
    name : String,
    crop : String,
    plotType : String,
    acre : String,
    prudate : String,
    drip : String,
    totalValves : String, 
});

module.exports = mongoose.model('plot', Plots);