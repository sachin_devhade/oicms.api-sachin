﻿/**
 * Created by user on 5/11/2016.
 */
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var slavesSchema = new schema({
    slaveId : String,
    scheduleNum : String,
    startTime : String,
    remainingTime : String,
    backflushTime : String,
    pwrStatus : String,
    pwrStatusTime : String,
    groupNum : String,
    valveNums : String,
    groupRuntime : String,
    sendTime : String
});


module.exports = mongoose.model('slave', slavesSchema);