﻿var mongoose = require('mongoose');
var schema = mongoose.Schema;

var Pumps = new schema({
    masterId : String,
    names : String,
});

module.exports = mongoose.model('pump', Pumps);