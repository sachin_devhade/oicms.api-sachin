/**
 * Created by user on 5/12/2016.
 */
var mongoose=require('mongoose');
var schema=mongoose.Schema;

var scheduleSchema=new schema({

    masterId: String,
    scheduleNum: String,    
    startTime: String,
    bf: String,
    backflushRuntime: String,
    status: String,
    totalGroups: String,
    groupDetails: [
        {
            pumpNumbers: String,
            totalValve: String,
            valveNumbers: String,
            runtime: String,
            _id : false
        }
    ],
    applyingDates: [
        {
            monthYear: String,
            dates: String,            
            backflush: String,
            skipped : String,
            _id : false
        }
    ]

});

module.exports=mongoose.model('schedule', scheduleSchema);