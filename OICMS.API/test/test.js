/**
 * Created by user on 5/18/2016.
 */

var request = require("request"),
    assert = require('assert'),
    base_url = "http://localhost:3000/skipschedule/controller/112";

describe("OICMS Server", function() {
    describe("GET /", function() {
        it("returns status code 200", function(done) {
            request.get(base_url, function(error, response, body) {
                //expect(response.statusCode).toBe(200);
                assert.equal(200, response.statusCode);
                done();
            });
        });

        it("returns 2", function(done) {
            request.get(base_url, function(error, response, body) {
                //expect(body).toBe("Hello World");
                assert.equal('"2"', body);
                done();
            });
        });
    });
});