﻿var express = require('express');
var router = express.Router();
var User = require('../models/user.server.model');
var Schedule = require('../models/schedule.server.model.js');
// Get Homepage
router.get('/', ensureAuthenticated, function (req, res) {
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('dashboard');
});

router.get('/customers', ensureAuthenticated, function (req, res) {
    User.find({}, function (err, users) { 
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('customers', {'customers' : users});
    });    
});

router.get('/customers/:masterId', ensureAuthenticated, function (req, res) {
    User.findOne({ '_id' : req.params.masterId }, function (err, user) {
        if (user) {
            res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
            res.render('editcust', { 'editcustomer' : user });
        }
        else {
            res.render('error', {
                message: "Incorrect Master ID",
                error: {}
            });
        }
    });
});

router.get('/customers/view/:masterId', ensureAuthenticated, function (req, res) {
    User.findOne({ '_id' : req.params.masterId }, function (err, user) {
        if (user) {
            Schedule.find({ 'masterId' : req.params.masterId }, function (err, schedules) { 
                res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
                res.render('viewcust', { 'customer' : user, 'schedules' : schedules });
            });            
        }
        else { 
            res.render('error', {
                message: "Incorrect Master ID",
                error: {}
            });
        }
    });
});

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.redirect('/admin/login');
    }
}

module.exports = router;