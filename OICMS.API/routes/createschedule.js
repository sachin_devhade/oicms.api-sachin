
//New code done by Sachin --->

var express=require('express');
var router=express.Router();
var Schedule = require('../models/schedule.server.model.js');
var ScheduleCtrl = require('../controllers/schedule.server.controller.js');

router.post('/app', function (req, res) {
    return ScheduleCtrl.createSchedule(req, res);
});

//router.get('/controller/:masterId/:scheduleNum', function (req, res) {
//    Schedule.findOne({ 'masterId': req.params.masterId, 'status': 'created','scheduleNum':req.params.scheduleNum }, function (err, schedules) {
//        if (err || !schedules) { 
//            res.json({'error': "No Data Found"});
//        }
//        else{        
//        var response = '*' + schedules.masterId + '!13CRT SCHD(' + schedules.scheduleNum + '){00/00/0000,00/00/0000,' + schedules.startTime + '}P(0){0},BF(' + schedules.onOff + '){00:00,' + schedules.backflushRuntime + '},VS(' + schedules.totalGroups + ')';
//            var pumpselection = "";
//            for (var i = 0; i < schedules.groupDetails.length; i++) {
//                var groupno = i + 1 < 10?'0' + (i + 1):(i + 1) + '';
//                var pumps = schedules.groupDetails[i].pumpNumbers.split(',').length;
//                var totalPumps = pumps < 10? '0' + pumps:pumps + '';
//                response += '{V(' + schedules.groupDetails[i].totalValve + '){' + schedules.groupDetails[i].valveNumbers + '}' + schedules.groupDetails[i].runtime + ';}';
//                pumpselection += '{PS(' + groupno + ')(' + totalPumps + '){' + schedules.groupDetails[i].pumpNumbers + '};}';
//            }
//        response += 'P'+ pumpselection + 'LRC&';
//        res.json(response);
//        }    
//    }
//    );
//});

router.get('/controller/:masterId', function (req, res) {
    Schedule.find({ 'masterId': req.params.masterId, 'status': 'ToCreate' }, function (err, schedules) {
        if (err || !schedules) { 
            res.json({'error': "No Data Found"});
        }
        else{
            for (var i = 0; i < schedules.length; i++) { 
                Schedule.findOneAndUpdate({ 'masterId' : schedules[i].masterId, 'scheduleNum' : schedules[i].scheduleNum },
                {
                    $set: { "status": "Created" }
                },
                 { new: true },
                 function (err, doc) {  });
            }
            res.json(schedules);
        }    
    });
});

module.exports = router;