
//New code done by Sachin ---->

var express = require('express');
var router = express.Router();
var Schedule = require('../models/schedule.server.model.js');
var ScheduleCtrl = require('../controllers/schedule.server.controller.js');
var bodyParser = require('body-parser');

router.post('/app/:masterId/:scheduleNum', function (req, res) {
    return ScheduleCtrl.applySchedule(req,res);    
});

//router.get('/controller/:masterId/:scheduleNum', function (req, res) {
//    Schedule.findOne({ 'masterId': req.params.masterId, 'status': 'applied', 'scheduleNum' : req.params.scheduleNum }, function (err, schedules) {
//        console.log(schedules);
//        if (err || !schedules) {
//            res.json({ 'error': "No Data Found" });
//        }
//        else{
//        var obj = schedules.applyingDates;
//        var response = '*'+ schedules.masterId +'!13ACT SCHD(' + schedules.scheduleNum + '){';
//        for (var i = 0; i < obj.length; i++) {
//                var noOfDays = obj[i].dates.split(',').length;
//                noOfDays = noOfDays < 10 ? '0' + noOfDays:noOfDays + '';
//            response += '{' + obj[i].monthYear + 'D(' + noOfDays + '){' + obj[i].dates + '}BF{' + obj[i].backflush + '}};';
//        }
//        response += '}LRC&';
//            res.json(response);
//        }
//    }
//    );
//});

router.get('/controller/:masterId', function (req, res) {
    Schedule.find({ 'masterId': req.params.masterId, 'status': 'ToApply' }, function (err, schedules) {
        if (err || !schedules) {
            res.json({ 'error': "No Data Found" });
        }
        else {
            for (var i = 0; i < schedules.length; i++) {
                Schedule.findOneAndUpdate({ 'masterId' : schedules[i].masterId, 'scheduleNum' : schedules[i].scheduleNum },
                {
                    $set: { "status": "Applied" }
                },
                 { new: true },
                 function (err, doc) { });
            }
            res.json(schedules);
        }
    });
});

module.exports = router;