
//code by Sachin -->

var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user.server.model.js');
var Pumps = require('../models/pumps.server.model.js');
var UserCtrl = require('../controllers/user.server.controller.js');

router.get('/', function(req, res, next) {
  User.find({}, function(err,users){
        res.json(users);
      });
});

router.post('/checkusername', function (req, res, next) {
    User.findOne({'username' : req.body.username}, function (err, user) {
        if (user)
            res.json(true); 
        else
            res.json(false);
    });
});

router.post('/editslave', function (req, res, next) { 
    return UserCtrl.editSlave(req, res);
});

router.post('/editpump', function (req, res, next) {
    return UserCtrl.editPump(req, res);
});

router.post('/login', function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    User.getUserByUsername(username, function (err, user) {
        if (err) throw err;
        else if (!user) {
            return res.json({ 'validUser': false, 'message': 'Invalid Username' });
        }
        else if (password == user.password) {
            var result = {
                _id : user._id,
                slaves : user.slaves,
                validUser : true,
                message : user.username
            }
            Pumps.findOne({'masterId' : user._id}, function (error, pump) {
                if (error) {
                    return res.json(result);
                }
                else {
                    result.pumps = pump.names;
                    return res.json(result);
                }
            });            
        }
        else
        return res.json({ 'validUser': false, 'message': 'Incorrect Password' });
    });
});

router.post('/newuser', ensureAuthenticated, function (req, res) {
    return UserCtrl.createUser(req, res);
});

router.post('/updateuser', ensureAuthenticated, function (req, res) {
    return UserCtrl.updateUser(req, res);
});

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        //req.flash('error_msg','You are not logged in');
        res.redirect('/admin/login');
    }
}

module.exports = router;
