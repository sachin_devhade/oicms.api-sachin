
//code by Sachin --->

var express = require('express');
var router = express.Router();
var Schedule = require('../models/schedule.server.model.js');
var ScheduleCtrl = require('../controllers/schedule.server.controller.js');
var bodyParser = require('body-parser');

router.post('/app/:masterId/:scheduleNum', function (req, res) {
    return ScheduleCtrl.skipSchedule(req, res);
});

//router.get('/controller/:masterId/:scheduleNum', function (req, res) {
//    Schedule.findOne({ 'masterId': req.params.masterId, 'status': 'Applied', 'scheduleNum' : req.params.scheduleNum }, function (err, schedules) {
//        if (err || !schedules) {
//            res.json(false);
//        }
//        else {
//            var d = new Date();
//            var dt = d.getDate();
//            var mm = d.getMonth() + 1;
//            mm = mm < 10? '0' + mm : mm + '';
//            var yyyy = d.getFullYear();            
//            var monthYear = yyyy + '/' + mm;
//            var flag = 0;
//            for (var i = 0; i < schedules.applyingDates.length; i++)
//            {
//                if (schedules.applyingDates[i].monthYear == monthYear)
//                {
//                    var dateArray = schedules.applyingDates[i].dates.split(',');
//                    var skipArray = schedules.applyingDates[i].skipped.split(',');
//                    for (var j = 0; j < dateArray.length; j++) {
//                        if (parseInt(dt) == parseInt(dateArray[j]) && skipArray[j] == 'S') {
//                            flag = 1;
//                            break;
//                        }
//                    }
//                }
//            }
//            if (flag == 1) {
//                var response = '*13SKIP SCH(' + schedules.scheduleNum + ')LRC&';
//                res.json(response);
//            }
//            else
//                res.json('Not Skipped Today');        
//        }
//    }
//    );
//});

router.get('/controller/:masterId', function (req, res) {
    Schedule.find({ 'masterId': req.params.masterId, 'status': 'Applied'}, function (err, schedules) {
        if (err || !schedules) {
            res.json(false);
        }
        else {
            var result = [];
            var d = new Date();
            var dt = d.getDate();
            var mm = d.getMonth() + 1;
            mm = mm < 10? '0' + mm : mm + '';
            var yyyy = d.getFullYear();
            var monthYear = yyyy + '/' + mm;
            var flag = 0;
            for (var i = 0; i < schedules.length; i++) {                
                for (var j = 0; j < schedules[i].applyingDates.length; j++) {
                    if (schedules[i].applyingDates[j].monthYear == monthYear) {
                        var dateArray = schedules[i].applyingDates[j].dates.split(',');
                        var skipArray = schedules[i].applyingDates[j].skipped.split(',');
                        for (var k = 0; k < dateArray.length; k++) {
                            if (parseInt(dt) == parseInt(dateArray[k]) && skipArray[k] == 'S') {
                                result.push({scheduleNum : schedules[i].scheduleNum});
                                flag = 1;
                                break;
                            }
                        }
                    }
                }
            }           
            if (flag == 1) {
                res.json(result);
            }
            else
                res.json('Not Skipped Today');
        }
    });
});

module.exports = router;
