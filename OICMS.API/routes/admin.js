﻿var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var Admin = require('../models/admin.server.model');
// Login
router.get('/login', function (req, res) {
    res.clearCookie('expresscookie');
    //res.cookie("expresscookie", "", { expires: new Date() });
    res.render('login', {title : 'Admin Login'});
});

router.post('/login',
  passport.authenticate('local', { successRedirect: '/', failureRedirect: '/admin/login', failureFlash: true }),
  function (req, res) {
    res.redirect('/');
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        Admin.getUserByUsername(username, function (err, user) {
            if (err) throw err;
            if (!user) {
                return done(null, false, { message: 'Unknown User' });
            }
            
            Admin.comparePassword(password, user.password, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, { message: 'Invalid password' });
                }
            });
        });
    }));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    Admin.getUserById(id, function (err, user) {
        done(err, user);
    });
});

router.get('/logout', function (req, res) {
    res.clearCookie('expresscookie');
    req.logout();
    req.session.destroy(function (err) {         
        res.redirect('/admin/login'); 
    });
});

module.exports = router;