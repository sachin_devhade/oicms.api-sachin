//Old Code Done by Chandan -->

//var express = require('express');
//var router = express.Router();
//var User=require('../models/user.server.model.js');
//var Ctrl_info=require('../models/ctrl_info.server.model.js');
//var bodyParser=require('body-parser');
//var Ctrl=require('../controllers/ctrl.server.controller.js');
//router.use(bodyParser.text());
//router.use(bodyParser.json());
//router.get('/controller/:ctrlId',function(req,res){
//    User.findOne(
//        {'ctrl.ctrlId': req.params.ctrlId },
//        function(err, user) {
//            if (err || !user || !user.ctrl || !user.ctrl.length) {
//                res.json({error: 'ctrl not found'});
//            } else {
//                var response="*SYS INFO{" + user.username + "," + user.password + "}57&";
//                res.send(response);
//            }
//        }
//    );
//});
//router.post('/controller/:ctrlId',function(req,res){
//    return Ctrl.createfile(req,res);
//});
//module.exports=router;

//New Code done by Sachin ---- >

var express = require('express');
var router = express.Router();
var User=require('../models/user.server.model.js');
var Sources = require('../models/sources.server.model.js');
var Plots = require('../models/plots.server.model.js');
var SettingCtrl = require('../controllers/setting.server.controller.js');
var bodyParser=require('body-parser');
router.use(bodyParser.text());
router.use(bodyParser.json());


router.post('/app', function (req, res) {
    return SettingCtrl.saveSetting(req,res);
});

router.post('/app/addsource', function (req, res) {
    return SettingCtrl.saveSource(req, res);
});

router.post('/app/deletesource', function (req, res) {
    Sources.remove({ masterId: req.body.masterId, _id: req.body.id }, function (err) {
        if (!err) {
            res.json(true);
        }
        else {
            res.json(false);
        }
    });
});

router.post('/app/deleteplot', function (req, res) {
    Plots.remove({ masterId: req.body.masterId, _id: req.body.id }, function (err) {
        if (!err) {
            res.json(true);
        }
        else {
            res.json(false);
        }
    });
});

router.get('/app/:masterId', function (req, res) {
    User.findOne({'_id' : req.params.masterId}, function (err, user) {
        res.json({
            pfccard : user.pfccard,
            pumps : user.pumps,
            filters : user.filters
        });
    }
    );
});


router.post('/app/addplot', function (req, res) {
    return SettingCtrl.savePlot(req, res);
});

router.get('/app/getsources/:masterId', function (req, res) {
    Sources.find({ 'masterId' : req.params.masterId }, function (err, sources) {
        res.json(sources);
    }
    );
});

router.get('/app/getplots/:masterId', function (req, res) {
    Plots.find({ 'masterId' : req.params.masterId }, function (err, sources) {
        res.json(sources);
    }
    );
});

router.post('/activate', function (req, res) {
    return SettingCtrl.activate(req, res);
});

router.post('/updatestatus', function (req, res) {
    return SettingCtrl.updatestatus(req, res);
});

module.exports=router;