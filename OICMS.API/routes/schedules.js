﻿var express = require('express');
var router = express.Router();
var Schedule = require('../models/schedule.server.model.js');
var Plots = require('../models/plots.server.model.js');

router.get('/', function (req, res, next) {
    Schedule.find({}, function (err, schedules) {
        res.json(schedules);
    }
    );
});

router.get('/cancreate/:masterId', function (req, res, next) {
    Schedule.findOne({ 'masterId': req.params.masterId }).sort('-scheduleNum').exec(function (err, schedules) {
        if (!schedules) {
            Plots.find({ masterId : req.params.masterId }, function (err1, plots) {
                if (plots.length > 0) { res.json({ 'new': true }); }
                else {
                    res.json({ 'new' : false, 'result': false, 'message': 'Please add Plots!' });
                }
            });           
        }
        else if (parseInt(schedules.scheduleNum) < 20) {
            Plots.find({ masterId : req.params.masterId }, function (err1, plots) {
                if (plots.length > 0) {
                    res.json({ 'new' : false, 'result': true, 'scheduleNum' : schedules.scheduleNum });
                }
                else {
                    res.json({ 'new' : false, 'result': false, 'message': 'Please add Plots!' });
                }
            });           
        }
        else
        res.json({ 'new' : false,'result': false, 'message' : 'Maximum schedules created!' });
    }
    );
});

router.get('/getcreated/:masterId', function (req, res, next) {
    Schedule.find({ 'masterId': req.params.masterId, 'status' : 'Created' }, function (err, schedules) {
        if (schedules.length > 0)
            res.json(schedules);
        else
            res.json(false);
    });
});

router.get('/getapplied/:masterId', function (req, res, next) {
    Schedule.find({ 'masterId': req.params.masterId, 'status' : 'Applied' }, function (err, schedules) {
        if (schedules.length > 0)
            res.json(schedules);
        else
            res.json(false);
    });
});


module.exports = router;